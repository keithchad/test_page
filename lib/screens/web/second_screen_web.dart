import 'dart:ui';

import 'package:flutter/material.dart';

class SecondScreen extends StatelessWidget {
  const SecondScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(
          height: 30.0,
        ),
        const Text(
          "Drei einfache Schritte zu \n deinem neuen Mitarbeiter",
          style: TextStyle(
              color: Color(0xff4A5568), fontSize: 25.0, fontFamily: 'Lato'),
        ),
        const SizedBox(
          height: 30.0,
        ),
        Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  margin: EdgeInsets.only(top: 120.0),
                  child: Row(
                    children: [
                      const Text(
                        "1.",
                        style: TextStyle(
                            color: Color(0xff718096),
                            fontSize: 100.0,
                            fontFamily: 'Lato'),
                      ),
                      Container(
                        margin: const EdgeInsets.only(top: 60.0, left: 10.0),
                        child: const Text(
                          "Erstellen dein Lebenslauf",
                          style: TextStyle(
                              color: Color(0xff718096),
                              fontSize: 20.0,
                              fontFamily: 'Lato'),
                        ),
                      ),
                    ],
                  ),
                ),
                const Image(
                  image: AssetImage("assets/profile_data.png"),
                ),
              ],
            ),
          ],
        ),
        const SizedBox(
          height: 100.0,
        ),
        Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 50.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Image(
                    image: AssetImage("assets/about_me.png"),
                  ),
                  Container(
                    margin: const EdgeInsets.only(left: 80.0),
                    child: Row(
                      children: [
                        const Text(
                          "2.",
                          style: TextStyle(
                              color: Color(0xff718096),
                              fontSize: 100.0,
                              fontFamily: 'Lato'),
                        ),
                        Container(
                          margin: const EdgeInsets.only(top: 60.0, left: 10.0),
                          child: const Text(
                            "Erstellen ein Jobinserat",
                            style: TextStyle(
                                color: Color(0xff718096),
                                fontSize: 20.0,
                                fontFamily: 'Lato'),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
        const SizedBox(
          height: 110.0,
        ),
        Column(
          children: [
            Container(
              margin: const EdgeInsets.only(right: 130.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text(
                    "3.",
                    style: TextStyle(
                        color: Color(0xff718096),
                        fontSize: 100.0,
                        fontFamily: 'Lato'),
                  ),
                  const SizedBox(
                    height: 20.0,
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 60.0),
                    child: const Text(
                      "Wähle deinen neuen \n Mitarbeiter aus",
                      style: TextStyle(
                          color: Color(0xff718096),
                          fontSize: 20.0,
                          fontFamily: 'Lato'),
                    ),
                  ),
                ],
              ),
            ),
            const Image(
              image: AssetImage("assets/swipe_profiles.png"),
            ),
          ],
        ),
        const SizedBox(
          height: 60.0,
        ),
      ],
    );
  }
}
