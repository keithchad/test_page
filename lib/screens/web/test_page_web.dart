import 'package:flutter/material.dart';
import 'first_screen_web.dart';
import 'second_screen_web.dart';
import 'third_screen_web.dart';

enum WidgetMarker { first, second, third }

class TestPageWeb extends StatefulWidget {
  const TestPageWeb({Key? key}) : super(key: key);

  @override
  State<TestPageWeb> createState() => _TestPageState();
}

class _TestPageState extends State<TestPageWeb> {
  WidgetMarker selectedWidgetMarker = WidgetMarker.first;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        resizeToAvoidBottomInset: false,
        body: SafeArea(
          child: SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  height: 7.0,
                  decoration: const BoxDecoration(
                      gradient: LinearGradient(
                          colors: [Color(0xff319795), Color(0xff3182CE)])),
                ),
                Container(
                  height: 60.0,
                  width: 5000.0,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: const BorderRadius.only(
                          bottomLeft: Radius.circular(13.0),
                          bottomRight: Radius.circular(13.0)),
                      boxShadow: [
                        BoxShadow(
                            color: Colors.black.withOpacity(0.3),
                            spreadRadius: 0.5,
                            blurRadius: 4.0)
                      ]),
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: const [
                        Text("Login",
                            style: TextStyle(
                                color: Color(0xff319795), fontFamily: 'Lato')),
                      ],
                    ),
                  ),
                ),
                Container(
                  width: 5000.0,
                  decoration: const BoxDecoration(
                    color: Color(0xffEBF4FF),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: const [
                      Text("Deine Job \n  website",
                          style: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 45.0,
                              color: Color(0xff2D3748),
                              fontFamily: 'Lato')),
                      Image(
                        image: AssetImage("assets/shaking_hands.png"),
                      ),
                    ],
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    OutlinedButton(
                      child: const Text(
                        "Arbeitnehmer",
                        style: TextStyle(
                            fontFamily: 'Lato', color: Color(0xff319795)),
                      ),
                      onPressed: () {
                        setState(() {
                          selectedWidgetMarker = WidgetMarker.first;
                        });
                      },
                    ),
                    OutlinedButton(
                      child: const Text(
                        "Arbeitgeber",
                        style: TextStyle(
                            fontFamily: 'Lato', color: Color(0xff319795)),
                      ),
                      onPressed: () {
                        setState(() {
                          selectedWidgetMarker = WidgetMarker.second;
                        });
                      },
                    ),
                    OutlinedButton(
                      child: const Text(
                        "Temporärbüro",
                        style: TextStyle(
                            fontFamily: 'Lato', color: Color(0xff319795)),
                      ),
                      onPressed: () {
                        setState(() {
                          selectedWidgetMarker = WidgetMarker.third;
                        });
                      },
                    ),
                  ],
                ),
                Container(
                  child: getCustomContainer(),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget getCustomContainer() {
    switch (selectedWidgetMarker) {
      case WidgetMarker.first:
        return getFirstContainer();
      case WidgetMarker.second:
        return getSecondContainer();
      case WidgetMarker.third:
        return getThirdContainer();
    }
  }

  Widget getFirstContainer() {
    return const FirstScreen();
  }

  Widget getSecondContainer() {
    return const SecondScreen();
  }

  Widget getThirdContainer() {
    return const ThirdScreen();
  }
}
