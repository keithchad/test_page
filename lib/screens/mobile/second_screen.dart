import 'dart:ui';

import 'package:flutter/material.dart';

class SecondScreen extends StatelessWidget {
  const SecondScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const Text(
          "Drei einfache Schritte zu deinem neuen Mitarbeiter",
          style: TextStyle(),
        ),
        Column(
          children: [
            const Image(
              image: AssetImage("assets/profile_data.png"),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 100.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text(
                    "1.",
                    style: TextStyle(
                        color: Color(0xff718096),
                        fontSize: 100.0,
                        fontFamily: 'Lato'),
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 60.0),
                    child: const Text(
                      "Erstellen dein Lebenslauf",
                      style: TextStyle(
                          color: Color(0xff718096),
                          fontSize: 20.0,
                          fontFamily: 'Lato'),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
        Column(
          children: [
            const Image(
              image: AssetImage("assets/about_me.png"),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 100.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text(
                    "2.",
                    style: TextStyle(
                        color: Color(0xff718096),
                        fontSize: 100.0,
                        fontFamily: 'Lato'),
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 60.0),
                    child: const Text(
                      "Erstellen ein Jobinserat",
                      style: TextStyle(
                          color: Color(0xff718096),
                          fontSize: 20.0,
                          fontFamily: 'Lato'),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
        Column(
          children: [
            const Image(
              image: AssetImage("assets/swipe_profiles.png"),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 100.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text(
                    "3.",
                    style: TextStyle(
                        color: Color(0xff718096),
                        fontSize: 100.0,
                        fontFamily: 'Lato'),
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 60.0),
                    child: const Text(
                      "Wähle deinen neuen \n Mitarbeiter aus",
                      style: TextStyle(
                          color: Color(0xff718096),
                          fontSize: 20.0,
                          fontFamily: 'Lato'),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ],
    );
  }
}
