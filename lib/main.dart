import 'package:flutter/material.dart';
import 'package:test_page/screens/web/test_page_web.dart';
import 'test_page.dart';

void main() {
  runApp(const ResponsiveLayout(
      mobileBody: TestPage(), desktopBody: TestPageWeb()));
}

class ResponsiveLayout extends StatelessWidget {
  final Widget mobileBody;
  final Widget desktopBody;

  const ResponsiveLayout(
      {Key? key, required this.mobileBody, required this.desktopBody})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        if (constraints.maxWidth < 600) {
          return mobileBody;
        } else {
          return desktopBody;
        }
      },
    );
  }
}
